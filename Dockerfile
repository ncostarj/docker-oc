# Dockerfile to build PHP container images
# Based on PHP 7.2
############################################################

# Set the base image to PHP 7.2
FROM php:7.2-apache

# File Author / Maintainer
LABEL maintainer="Newton Gonzaga Costa<ncosta.rj@gmail.com>"
LABEL vendor="PHP"

VOLUME [ "/var/www/html" ]
WORKDIR /var/www/html

# Expose the default port
EXPOSE 80

# Basic packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    vim \
    curl \
    git \
    htop \
    cron \
    libfreetype6-dev \
    libmcrypt-dev \
    zlib1g-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    locales \
    && rm -rf /var/lib/apt/lists/*
    
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install zip gd pdo_mysql \
    && sed -i -- 's/# pt_BR.UTF-8 UTF-8/pt_BR.UTF-8 UTF-8/g' /etc/locale.gen \
    && locale-gen \
    && sed -i -- 's/Etc\/UTC/America\/Sao_Paulo/g' /etc/timezone \
    && sed -i -- 's/# alias ll/alias ll/g' /root/.bashrc \
    && unlink /etc/localtime \
    && ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
    && a2enmod rewrite \
    && echo "Listen 80\nServerName localhost" >> /etc/apache2/apache2.conf

ENV LC_ALL pt_BR.UTF-8

# Copy conf file
COPY dconf/000-default.conf /etc/apache2/sites-available
COPY dconf/php.ini /usr/local/etc/php
